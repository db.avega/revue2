import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test('User can log in', async ({ page }) => {
  await page.goto('/');

  await page.getByLabel("Din mail").fill("test@test.com");
  await page.getByLabel("Ditt lösenord").fill("Test123");
  await page.getByRole("button",{name:"Skicka"}).click();

  await expect(
    page.getByRole("heading", { name: "Modul 1" }).first()
  ).toBeVisible();
})
